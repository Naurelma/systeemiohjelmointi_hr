
#include <stdio.h>
#include <stdlib.h>



int readAndUnCompress(const char *);

int main(int argc, char const *argv[]){
	
	if (argc == 1){
		printf("Usage: %s file1 [file2 ...]", argv[0]);
		return 1;
	}
	/*
	I will skip unopenable files completely here, since that is undefined
	But return value will be 1 if at least 1 file was not opened
	*/
	int val = 0;
	for (int i = 1; i < argc; ++i){
		val = val + readAndUnCompress(argv[i]);
		//printf("\n");
		
	}

	/*
	!! converts any value to 1 or 0 (0 for 0 or NULL 1 for others)
	*/
	return !!val;
}


int readAndUnCompress(const char *name){
	FILE *f = fopen(name, "r");
	if (!f){
		fprintf(stderr,"File %s Cannot be opened!\n", name);
		/*Not clear if should exit now or read rest of the files*/
		//exit(1);
		return 1;
	}
	
	short counter;
	char curr;

	while(!feof(f)){
		fread(&counter, sizeof(counter), 1, f);
		fread(&curr, sizeof(curr), 1, f);
		//printf("c:%c d:%d\n",curr, counter );//testing
		
		for (int i = 0; i < counter; ++i){
			printf("%c", curr);
		}

	}

	fclose(f);

	return 0;
}
