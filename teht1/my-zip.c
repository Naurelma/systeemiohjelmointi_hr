
#include <stdio.h>
#include <stdlib.h>



int readAndCompress(const char *);

int main(int argc, char const *argv[]){
	
	if (argc == 1){
		printf("Usage: %s file1 [file2 ...]\n", argv[0]);
		
		return 1;
	}
	/*
	I will skip unopenable files completely here, since that is undefined
	But return value will be 1 if at least 1 file was not opened
	*/
	int val = 0;
	for (int i = 1; i < argc; ++i){
		val = val + readAndCompress(argv[i]);
		//printf("\n");
	}
	

	/*
	!! converts any value to 1 or 0 (0 for 0 or NULL 1 for others)
	*/
	return !!val;
}


int readAndCompress(const char *name){
	FILE *f = fopen(name, "r");
	if (!f){
		fprintf(stderr,"File %s Cannot be opened!\n", name);
		/*Not clear if should exit now or read rest of the files*/
		//exit(1);
		return 1;
	}
	
	short counter = 0;
	char curr, last=getc(f);
	ungetc(last,f);

	while((curr=getc(f)) != EOF){
		if (last != curr){
			//printf("%d%c", counter, last);//testing
			
			fwrite(&counter, sizeof(counter),1, stdout);
			fwrite(&last, sizeof(last), 1, stdout);
			
			last = curr;
			counter = 1;
		} else {
			//last == curr
			++counter;
		}

	}
	//write the last char beforre eof
	//printf("%d%c", counter, last);//testing
	fwrite(&counter, sizeof(counter),1, stdout);
	fwrite(&last, sizeof(last), 1, stdout);
			
	fclose(f);

	return 0;
}
