
#include <stdio.h>
#include <stdlib.h>

#define SIZE 16


int readAndPrint(const char *);

int main(int argc, char const *argv[]){
	
	if (argc == 1){
		printf("No files to print.\n");
		/*normal cat reads from stdin in this case,
		allows to be used in pipes*/
		return 0;/*1 or 0 here? */
	}
	
	int val = 0;
	for (int i = 1; i < argc; ++i){
		printf("\nPrinting: %s\n", argv[i]);
		val = val + readAndPrint(argv[i]);
	}
	printf("\n");

	/*
	!! converts any value to 1 or 0 (0 for 0 or NULL 1 for others)
	*/
	return !!val;
}


int readAndPrint(const char *name){
	FILE *f = fopen(name, "r");
	if (!f){
		fprintf(stderr,"File %s Cannot be opened!\n", name);
		/*Not clear if should exit now or read rest of the files*/
		exit(1);
		//return 1;
	}
	char buff[SIZE];

	while (fgets(buff, SIZE, f)){
		printf("%s", buff);
	}

	fclose(f);

	return 0;
}
