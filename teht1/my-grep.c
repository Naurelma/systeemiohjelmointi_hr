#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

#include <string.h>



int readAndPrintIf(const char *, FILE *);

int main(int argc, char const *argv[]){
	
	int val = 0;
	if (argc == 1){
		printf("Usage: %s searchterm [file ...]\n", argv[0]);
		return 1;

	} else if (argc == 2){
		val = readAndPrintIf(argv[1], stdin);

	} else {
		for (int i = 2; i < argc; ++i){
			printf("\nPrinting: %s\n", argv[i]);
			val = val + readAndPrintIf(argv[1], fopen(argv[i], "r"));
			/* counts up if errors, could be made to return the # of unopened files */
		}
		printf("\n");

	}
	/*
	!! converts any value to 1 or 0 (0 for 0 or NULL 1 for others)
	*/
	return !!val;
}


int readAndPrintIf(const char *target, FILE *f){
	if (!f){
		fprintf(stderr,"File %s Cannot be opened!\n", target);
		/*Not clear if should exit now or read rest of the files*/
		exit(1);
		//return 1;
	}

	char *lineptr = NULL;
	size_t size = 0;
	

	while (getline(&lineptr, &size, f) != -1){
		/*Loop over all the lines*/
		if (strstr(lineptr, target)){
			/*Test if the line contains the searchterm*/
			printf("%s", lineptr);
		}
	}
	/*man page told to free the line after*/
	free(lineptr);


	fclose(f);

	return 0;
}
