
#include "types.h"
#include "user.h"

/*
this file is for testing the added syscall
*/

/* 
edits done:
	added a line to usys.S
	added num to syscall.h
	added func to syscall.c
	added counter to syscall.c
	added signature to user.h
	func is written in sysfile.c at the end
	added this file to makefile

the followed sys call can be changed by arguments now
*/


/*
use with no args to get the default behaviour, use with positive int as first arg and you get that syscalls counter
use with a negative number and that syscall counter is reset
*/

int main(int argc, char const *argv[]){
	int count = 1;
	if (argc == 1){
		count = getreadcount();
		printf(1,"read has been called %d times\n", count);
	} else{

		int num = atoi(argv[1]);
		count = getcallcount(num);

		printf(1,"syscall %d has been called %d times\n", num, count);
	}
	exit();
	
}
