
/*
missing: paraller, redirection
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>


#define ERROR fprintf(stderr, "An error has occurred\n")

//this will flood the screen with additional info
//#define DEBUG 




void repl(FILE *, const char *);
char* concat(const char *, const char *);


int main(int argc, char const *argv[]){
	if (argc == 1){
		repl(stdin, "wish> ");
	} else{
		FILE *f = fopen(argv[1], "r");
		if(f){
			repl(f, "");
		} else{
			ERROR;
			printf("Cannot open %s\n", argv[1]);
		}
	}
	return 0;
}


/*Read Eval Print Loop*/
void repl(FILE *f, const char *promt){
	/*should probably break this into multible functions */
	char *lineptr = NULL;
	
	char *path = strdup("/bin/"); /*default path*/
	char *tokenized, *duplineptr;
	char *command, *tmp, *rest;
	const char delim[4] = " \n\t";
	const char amp[2] = "&";
	const char gt[2] = ">";
	size_t size = 0;
	
	ssize_t read; /*len of read line*/

	do{/* main loop*/
		printf("%s", promt);
		fflush(f);
		read = getline(&lineptr, &size, f);
		/*Pure new line, do nothing*/
		if(read <= 1){
			continue;
		}

		duplineptr = strdup(lineptr);
		while((tokenized = strsep(&duplineptr, amp)) != NULL){
			command = strsep(&tokenized, delim);
			if(strlen(command) == 0){
				/*Double space*/
				continue;
			}

			/*cut the string at > for redirect*/
			tmp = strsep(&tokenized, gt);
			rest = tokenized;
			tokenized = tmp;


			/*Buildin commands */
			if (strcmp(command, "exit") == 0){
				exit(0);

			} else if(strcmp(command, "cd") == 0){
				tmp = strsep(&tokenized, delim);
				if(chdir(tmp)){
					/*
					man page said that 0 on succesees, 
					but i only get 0 for errors...
					*/
					ERROR;
				}
				#ifdef DEBUG
					/*DEBUG, on succeses these should be equal*/
					printf("kohde: \n%s\n", tmp);
					system("pwd");
				#endif
				
				continue;

			} else if(strcmp(command, "path") == 0){
				free(path);
				path = strdup(tokenized);
				/* Tokenize it later*/
				
				continue;

			} 

			/*external commands */
			
			
			#ifdef DEBUG
				printf("Parents id: %d\n", getpid());
			#endif

			int rc = fork();
			if (rc<0){
				printf("Failed to fork!!");
				ERROR;
			 	exit(1);

			} else if(rc == 0){ /*Child*/
				char *commandPath;
				char *tmpPath = strdup(path); 
				/*
				Does not get freed, gets replaced with execv
				*/
					
				#ifdef DEBUG
					printf("Full path: %s\n", path);
			
					printf("Child id %d For command %s\n", getpid(), command);
				#endif

				while((tmp = strsep(&tmpPath, delim)) != NULL) {
					/*loop over paths */
					if(strlen(tmp) == 0){
						/*
						assume that empty path is not valid
						Here they are artifacts of strsep
						*/
						continue;
					}
					
					#ifdef DEBUG
						printf("Path to test: %s\n", tmp);
					#endif

					if (tmp[strlen(tmp)-1] == '/'){
						commandPath = concat(tmp, command);

					} else{
						#ifdef DEBUG
							printf("Adding /\n");
						#endif
						/*if the user didn't add the / */
						tmp = concat(tmp,"/");
						commandPath = concat(tmp, command);
						free(tmp);
					}

					#ifdef DEBUG
						printf("Testing command:%s\n",commandPath);
					#endif

					if (!access(commandPath, X_OK)){
						#ifdef DEBUG
							printf(" Running!\n");
						#endif
						/*Construct the args list*/

						
						size_t w = 8;
						char **argList = (char **) malloc(sizeof(char**)*(w+1));

						argList[0] = command;
						int i = 1;
						while ((tmp = strsep(&tokenized, delim))){
							if(strlen(tmp) == 0){
								/*
								assume that 0 length arguments are invalid
								Here they are artifacts of strsep
								*/
								continue;
							}
							argList[i] = tmp;
							++i;
							if(i > w){
								w = w+8;
								argList = realloc(argList, sizeof(char**)*(w+1));
							}
						}
						argList[i] = NULL;



						#ifdef DEBUG
							printf(" Running!\n");
							printf("Calling Now:\n");
						#endif
						printf("\n");

						/*Configure redirect*/
						
						tmp = NULL;

						while((tmp = strsep(&rest, delim))){
							if(strlen(tmp) == 0){
								continue;
							}
							break;
							/*
							ls >file file2
							will just redirect to file and 
							silently ignore file2
							*/
							
						}

						if(tmp){		
							#ifdef DEBUG
								printf("Redirecting to: %s\n", tmp);
							#endif
							freopen(tmp,"w",stdout);		
						
						}

						execv(commandPath, argList);
						/* 
						thecnically should free arglist, 
						but this never happens
						*/
					}
					#ifdef DEBUG
						else{
							printf(" Failed\n");
						}
					#endif
					free(commandPath);
				}
				ERROR;
				printf("Command \"%s\" not found in path.\n", command);
			 	exit(0);
			 	/*
			 	Safeguard against errors in the loop
			 	To avoid fork bomb
			 	*/

			} else{ /*Parent*/
			 	/*atm just testing */
			 	/*parent goes to next command*/
			}
		}/*Paraller loop*/

		while(wait(NULL) > 0){
		};
		/* wait for all children*/

		
		#ifdef DEBUG
			printf("\n");
		#endif
		free(duplineptr);

	} while(read != -1); /*eof detection */
	printf("\n%s\n","Stopping now." );

	/*man page told to free the line after*/
	free(lineptr);

}

char* concat(const char *s1, const char *s2){
	/*Source: 
	https://stackoverflow.com/questions/8465006/how-do-i-concatenate-two-strings-in-c/8465083 
	*/
	const size_t len1 = strlen(s1);
	const size_t len2 = strlen(s2);
	char *result = malloc(len1 + len2 + 1);
	memcpy(result, s1, len1);
	memcpy(result + len1, s2, len2 + 1); 
	return result;
}

